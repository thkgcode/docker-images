#!/bin/sh

# Generates keys if not already present
ssh-keygen -A

if [ ! -z "${ROOT_PASSWORD}" ]; then
  echo "root:${ROOT_PASSWORD}" | chpasswd
fi

echo "IP: $(hostname -i)"

# do not detach (-D), log to stderr (-e), passthrough other arguments
exec /usr/sbin/sshd -D -e "$@"
