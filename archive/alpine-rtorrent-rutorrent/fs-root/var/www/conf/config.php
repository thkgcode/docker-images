<?php
    // configuration parameters

    // for snoopy client
    @define('HTTP_USER_AGENT', 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0', true);
    // in seconds
    @define('HTTP_TIME_OUT', 30, true);

    @define('HTTP_USE_GZIP', true, true);

    $httpIP = null; // IP string. Or null for any.

    // in seconds
    @define('RPC_TIME_OUT', 5, true);

    @define('LOG_RPC_CALLS', false, true);
    @define('LOG_RPC_FAULTS', true, true);

    // for php
    @define('PHP_USE_GZIP', false, true);
    @define('PHP_GZIP_LEVEL', 2, true);

    // rand for schedulers start, +0..X seconds
    $schedule_rand = 10;

    $do_diagnostic = true;

    // path to log file (comment or leave blank to disable logging)
    $log_file = '/var/log/php/rutorrent.log';

    // Save uploaded torrents to profile/torrents directory or not
    $saveUploadedTorrents = true;

    // Overwrite existing uploaded torrents in profile/torrents
    // directory or make unique name
    $overwriteUploadedTorrents = false;

    // Upper available directory. Absolute path with trail slash.
    $topDirectory = '/';
    $forbidUserSettings = false;

    $scgi_port = 5000;
    $scgi_host = "127.0.0.1";

    // For web->rtorrent link through unix domain socket
    // (scgi_local in rtorrent conf file), change variables
    // above to something like this:
    //
    // $scgi_port = 0;
    // $scgi_host = "unix:///tmp/rpc.socket";

    // DO NOT DELETE THIS LINE!!! DO NOT COMMENT THIS LINE!!!
    $XMLRPCMountPoint = "/RPC2";

    // If empty, will be found in PATH.
    $pathToExternals = array(
        "php"  => '/usr/local/bin/php',
        "curl" => '/usr/bin/curl',
        "gzip" => '',
        "id"   => '',
        "stat" => '',
    );

    // list of local interfaces
    $localhosts = array(
        "127.0.0.1",
        "localhost",
    );

    // Path to user profiles
    $profilePath = '/data/rutorrent';
    // Mask for files and directory creation in user profiles.
    $profileMask = 0777;
    // Both Webserver and rtorrent users must have read-write access to it.
    // For example, if Webserver and rtorrent users are in the same
    // group then the value may be 0770.

    // Temp directory. Absolute path with trail slash. If null,
    // then autodetect will be used.
    $tempDirectory = null;

    // Use X-Sendfile feature if it exist
    $canUseXSendFile = true;

    $locale = "UTF8";
