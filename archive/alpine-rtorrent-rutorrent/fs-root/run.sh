#!/bin/sh

_trap() {
  CODE=$?
  pkill -${CODE} rtorrent
  echo "Stopping rtorrent..."
  wait $!
  echo "Exiting..."
  exit 0
}


# Catch most-common signals sent to stop container.
trap _trap HUP INT QUIT TERM

mkdir -p \
  /data/rtorrent/session \
  /data/rtorrent/torrents \
  /data/rtorrent/incomplete \
  /data/rtorrent/completed \
  /data/rutorrent/torrents \
  /var/log/php

chmod -R a+w /data

chown nobody:nobody \
  /var/log/php \
  /data/rutorrent

rm -f /data/rtorrent/session/rtorrent.lock

# Start services
php-fpm -y /etc/php-fpm.conf
nginx
screen -d -m -S rtorrent rtorrent

while true; do
  sleep 10 &
  wait $!
done
