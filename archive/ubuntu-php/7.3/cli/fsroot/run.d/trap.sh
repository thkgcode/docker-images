#!/bin/bash

_trap() {
  # @todo switch to more standard solution for managing services
  echo "Exiting..."
  exit 0
}

# Catch most-common signals sent to stop container.
trap _trap HUP INT QUIT TERM
