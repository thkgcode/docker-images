#!/bin/bash

CMDS=$(env | grep CMD_ | sed -r 's/^CMD_([A-Z0-9_]+)=(.*)$/\1 \2/')
IFS=$'\n'
for CMD in $CMDS; do
    IFS=' '
    CMD=($CMD)
    COMMAND="${CMD[@]:2}"
    su -c "${COMMAND}" - "${CMD[1]}" &
done
