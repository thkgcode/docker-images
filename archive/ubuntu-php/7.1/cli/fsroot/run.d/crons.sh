#!/bin/bash

CRONS=$(env | grep CRON_ | sed -r 's/^CRON_([A-Z0-9_]+)=(.*)$/\1 \2/')

rm -rf /var/spool/cron/env-*
IFS=$'\n'
for CRON in $CRONS; do
    set -f
    IFS=' '
    CRON=($CRON)
    USER="${CRON[1]}"
    COMMAND="${CRON[@]:2}"

    echo "${COMMAND}" >> "/var/spool/cron/env-${USER}"
    set +f
done

cron
