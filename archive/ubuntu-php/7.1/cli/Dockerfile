FROM ubuntu:18.04

ENV PHP_VERSION="7.1"

# Build dependencies
ARG DEP_BUILD="gnupg"

# Runtime dependencies
ARG DEP_RUNTIME="\
    ca-certificates \
    cron \
    openssl \
    php-pear \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-cli \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-mcrypt \
    php${PHP_VERSION}-mysql \
    php${PHP_VERSION}-pgsql \
    php${PHP_VERSION}-redis \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-sqlite \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-zip \
"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install --no-install-recommends -y ${DEP_BUILD} \
 && apt-key adv --keyserver keyserver.ubuntu.com --recv-key 4F4EA0AAE5267A6C \
 && echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu bionic main" > /etc/apt/sources.list.d/ondrej.list \
 && apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install --no-install-recommends -y ${DEP_RUNTIME} \
 && apt-get purge -y ${DEP_BUILD} \
 && apt-get autoremove -y \
 && dpkg --get-selections | grep deinstall | awk '{ print $1 }' | xargs apt-get purge -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

ARG DEP_MYSQL_BUILD="php${PHP_VERSION}-dev make curl"

RUN apt-get update \
 && apt-get install --no-install-recommends -y ${DEP_MYSQL_BUILD} \
 && mkdir /tmp/mysql -p \
 && curl -L https://github.com/php/pecl-database-mysql/archive/master.tar.gz -o /tmp/mysql/mysql.tgz \
 && cd /tmp/mysql \
 && tar xf mysql.tgz \
 && cd /tmp/mysql/pecl-database-mysql-master \
 && phpize \
 && ./configure \
 && make install \
 && apt-get purge -y ${DEP_MYSQL_BUILD} \
 && apt-get autoremove -y \
 && dpkg --get-selections | grep deinstall | awk '{ print $1 }' | xargs apt-get purge -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN useradd -s /bin/bash -u 1000 user \
 && mkdir -p /home/user \
 && chown 1000:1000 /home/user
CMD bash /run.sh
ADD fsroot /
