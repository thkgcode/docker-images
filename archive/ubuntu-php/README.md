PHP 7.2 Docker Images
===

Supported PHP Versions
---

* 7.2

Images
---

For each version of PHP supported, the following images are provided.

* cli
* fpm
* fpm-debug
* build


Future Plans
---

* Adding cgi/cgi-debug images (useful for PHP-PM)
* Adding swoole/swoole-debug images
