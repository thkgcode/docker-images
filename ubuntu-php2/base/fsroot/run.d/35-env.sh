#!/bin/bash

# Causes all env variables prefixed with FOOS_ to be written to /my/foos/.env
# ENV_FOOS = /my/foos/.env
# FOOS_DB_NAME = asdf

set -e

ENVS=$(env | grep ENV_ | sed -r 's/^ENV_([A-Z0-9_]+)=(.*)$/\1 \2/')
IFS=$'\n'
for ENV in $ENVS; do
    IFS=' '
    ENV=($ENV)

    ENV_NAME="${ENV[0]}"
    FILE="${ENV[1]}"

    VARS=$(env | grep "${ENV_NAME}_" | sed -r "s/^${ENV_NAME}_([A-Z0-9_]+)=(.*)$/\1=\2/")

    echo "${VARS}" > "${FILE}"
done


