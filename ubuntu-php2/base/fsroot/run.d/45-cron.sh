#!/bin/bash

set -e

CRONS=$(env | grep CRON_ | sed -r 's/^CRON_[A-Z0-9_]+=(.*)$/\1/')
CRONFILE=/etc/cron.d/dockercron

rm -rf "${CRONFILE}"
IFS=$'\n'
for CRON in $CRONS; do
    echo "${CRON}" >> "${CRONFILE}"
done

cron
