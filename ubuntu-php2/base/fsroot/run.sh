#!/bin/bash

set -e

for FILE in /run.d/*.sh; do
  chmod +x $FILE
  $FILE || exit 1
done

# This allows trapping signals to work
# (while using tail against /dev/null doesn't)
while true; do
  sleep 10 &
  wait $!
done
