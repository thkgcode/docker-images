#!/bin/bash

# Causes all env variables prefixed with FOOS_ to be written to /my/foos/.env
# POOL_FOOS = /my/foos/.env
# FOOS_DB_NAME = asdf

set -e

touch /var/log/php/app.{access,error,slow}.log

chown -R www-data:www-data /var/log/php

POOLS=$(env | grep POOL_ | sed -r 's/^POOL_([A-Z0-9_]+)=(.*)$/\1 \2/')
IFS=$'\n'

if [ -z "$POOLS" ]; then
    exit
fi

PHP_DIR=$(php -r 'echo dirname(PHP_CONFIG_FILE_PATH);')

for POOL in $POOLS; do
    IFS=' '
    POOL=($POOL)

    NAME="${POOL[0]}"
    PORT="${POOL[1]}"
    MAX_CHILDREN="${POOL[2]:-20}"

    ENV=$(env | grep "${NAME}_" | sed -r "s/^${NAME}_([A-Z0-9_]+)=(.*)$/env[\1] = \2/")

    FILE="${PHP_DIR}/fpm/pool.d/${NAME}.conf"
    cp /opt/fpm-pool.conf "${FILE}"

    sed -i "s#{{NAME}}#${NAME}#g" "${FILE}"
    sed -i "s#{{PORT}}#${PORT}#g" "${FILE}"
    sed -i "s#{{MAX_CHILDREN}}#${MAX_CHILDREN}#g" "${FILE}"
    sed -i "s#{{ENV}}#${ENV}#g" "${FILE}"
done

# @todo move to supervisord
php-fpm
