# Find directories with Makefiles
MAKEFILES = $(shell find . -mindepth 2 -maxdepth 2 -type f -name Makefile)

# Filter makefile matches to directory name
SUBDIRS   = $(subst /,,$(subst ./,,$(dir $(MAKEFILES))))

# append command to each directory
BUILD_TARGETS = $(patsubst %,build-%,$(SUBDIRS))
FRESH_TARGETS = $(patsubst %,fresh-%,$(SUBDIRS))
PULL_TARGETS = $(patsubst %,pull-%,$(SUBDIRS))
PUSH_TARGETS = $(patsubst %,push-%,$(SUBDIRS))
DESTROY_TARGETS = $(patsubst %,destroy-%,$(SUBDIRS))

# Strip prefix (build-, push-, etc.) to determine intended
# directory
TARGET_DIR = $(shell echo $@ | sed 's/[^-]*-//')

build: $(BUILD_TARGETS)

$(BUILD_TARGETS):
	$(MAKE) -C $(TARGET_DIR) build

fresh: $(FRESH_TARGETS)

$(FRESH_TARGETS):
	$(MAKE) -C $(TARGET_DIR) fresh

pull: $(PULL_TARGETS)

$(PULL_TARGETS):
	$(MAKE) -C $(TARGET_DIR) pull

push: $(PUSH_TARGETS)

$(PUSH_TARGETS):
	$(MAKE) -C $(TARGET_DIR) push

destroy: $(DESTROY_TARGETS)

$(DESTROY_TARGETS):
	$(MAKE) -C $(TARGET_DIR) destroy
