## Environment Configuration


### Hosts

```yml
# Proxy Example (proxies to apache:80)
# HOST_NAME: hostname proxy /path/to/webroot hostname port
HOST_EXAMPLE_SITE: example-site proxy /var/www/html apache 80

# FastCGI example (passes to fpm:9000)
# HOST_NAME: hostname fastcgi /path/to/webroot hostname port
HOST_EXAMPLE_SITE: example-site fastcgi /var/www/html fpm 9000

# Static example
# HOST_NAME: hostname static /path/to/webroot
HOST_EXAMPLE_SITE: example-site static /var/www/html
```


### SSL

Certs are assumed to be `/etc/letsencrypt`

```yml
# NAME_SSL: cert_name force_https?

HOST_EXAMPLE_SITE: example-site fastcgi /var/www/html fpm 9000
EXAMPLE_SITE_SSL: example-site 1
```


### Additional Config

When a host requires additional config, an environment variable
can be used to include an additional configuration file.

Includes are assumed to be mounted into `/etc/nginx/includes`

To load additional configuration from
`/etc/nginx/includes/example-site.conf`, use the following:

```yml
HOST_EXAMPLE_SITE: example-site fastcgi /var/www/html fpm 9000
EXAMPLE_SITE_INCLUDE: example-site
```


### Basic Auth

To restrict access to an entire host, basic auth can be used.

htpasswd files should be defined within `/etc/nginx/auth`

To restrict a host to users listed in
`/etc/nginx/auth/default.htpasswd`, use the following:

```yml
HOST_EXAMPLE_SITE: example-site fastcgi /var/www/html fpm 9000
EXAMPLE_SITE_AUTH: default
```

