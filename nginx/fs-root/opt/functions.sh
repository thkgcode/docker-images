#!/bin/bash

check_crawlable() {
    CRAWL_VAR="${HOST[0]}_CRAWL"
    CRAWL="${!CRAWL_VAR}"

    if [ -z "${CRAWL}" ] || [ "${CRAWL}" = "0" ]; then
        CRAWL=disable
    else
        CRAWL=enable
    fi

    sed -i "s#{{CRAWL}}#${CRAWL}#g" "$DEST"
}

check_cacheable() {
    CACHE_VAR="${HOST[0]}_CACHE"
    CACHE="${!CACHE_VAR}"

    if [ "${CACHE}" = "upstream" ]; then
        CACHE=upstream
    elif [ "${CACHE}" = "max" ]; then
        CACHE=max
    else
        CACHE=none
    fi

    sed -i "s#{{CACHE}}#${CACHE}#g" "$DEST"
}

check_maintenance() {
    MAINTENANCE_VAR="${HOST[0]}_MAINTENANCE"
    MAINTENANCE="${!MAINTENANCE_VAR}"

    if [ "${MAINTENANCE}" = "1" ]; then
        MAINTENANCE=enable
    else
        MAINTENANCE=disable
    fi

    sed -i "s#{{MAINTENANCE}}#${MAINTENANCE}#" "$DEST"
}

http_serve() {
    VHOST_SRC="/opt/virtual-hosts/http.serve.conf"
    VHOST_DEST="/etc/nginx/conf.d/${HOST[1]}.http.conf"
    cp "$VHOST_SRC" "$VHOST_DEST"
    sed -i "s#{{VHOST}}#${HOST[1]}#g" "$VHOST_DEST"
    sed -i "s#{{TEMPLATE}}#${DEST}#g" "$VHOST_DEST"
}

http_secure() {
    VHOST_SRC="/opt/virtual-hosts/http.secure.conf"
    VHOST_DEST="/etc/nginx/conf.d/${HOST[1]}.http.conf"
    cp "$VHOST_SRC" "$VHOST_DEST"
    sed -i "s#{{VHOST}}#${HOST[1]}#g" "$VHOST_DEST"
}

https() {
    VHOST_SRC="/opt/virtual-hosts/https.conf"
    VHOST_DEST="/etc/nginx/conf.d/${HOST[1]}.https.conf"

    SSL_VAR="${HOST[0]}_SSL"
    SSL="${!SSL_VAR}"

    if [ -z "${SSL}" ]; then
        http_serve
        return
    fi

    cp "$VHOST_SRC" "$VHOST_DEST"

    IFS=" " read -r -a SSL <<< "$SSL"
    SSL_PATH="/etc/letsencrypt/live/${SSL[1]:-default}"

    if [ ! -d "${SSL_PATH}" ]; then
        ssl_gen "${SSL[1]:-default}"
    fi

    sed -i "s#{{VHOST}}#${HOST[1]}#g" "$VHOST_DEST"
    sed -i "s#{{TEMPLATE}}#${DEST}#g" "$VHOST_DEST"
    sed -i "s#{{SSL_PATH}}#${SSL_PATH}#g" "$VHOST_DEST"

    if [ ! -z "${DH_PARAMS}" ]; then
        sed -i "s#{{DH_CONFIG}}#include /opt/snippets/dh_params.conf;#g" "$VHOST_DEST"
    else
        sed -i "s#{{DH_CONFIG}}##g" "$VHOST_DEST"
    fi

    if [ "${SSL[0]}" = "0" ]; then
        http_serve
    else
        http_secure
    fi
}

upstream() {
    UPSTREAM_NAME="${HOST[0]}"
    UPSTREAM_SERVER="${HOST[4]}:${HOST[5]}"

    UPSTREAM_SRC="/opt/upstream.conf"
    UPSTREAM_DEST="/etc/nginx/conf.d/upstream_${UPSTREAM_NAME}.conf"

    UPSTREAM_VAR="${HOST[0]}_UPSTREAM"
    UPSTREAM_COUNT="${!UPSTREAM_VAR}"

    echo "UPSTREAM_VAR: $UPSTREAM_VAR"
    echo "UPSTREAM_COUNT: $UPSTREAM_COUNT"

    if [ -z "${UPSTREAM_COUNT}" ]; then
        UPSTREAM_COUNT=20
    fi

    cp "$UPSTREAM_SRC" "$UPSTREAM_DEST"
    sed -i "s#{{UPSTREAM_NAME}}#${UPSTREAM_NAME}#g" "$UPSTREAM_DEST"
    sed -i "s#{{UPSTREAM_SERVER}}#${UPSTREAM_SERVER}#g" "$UPSTREAM_DEST"
    sed -i "s#{{UPSTREAM_COUNT}}#${UPSTREAM_COUNT}#g" "$UPSTREAM_DEST"
}

host_fastcgi() {
    if [ ! -d "${HOST[3]}" ]; then
      mkdir -p "${HOST[3]}"
    fi

    sed -i "s#{{WEBROOT}}#${HOST[3]}#g" "$DEST"
    sed -i "s#{{HOST}}#${HOST[0]}#g" "$DEST"
    sed -i "s#{{HOST_DOMAIN}}#${HOST_DOMAIN}#g" "$DEST"

    feature_auth
    feature_include

    upstream
}

host_proxy() {
    if [ ! -d "${HOST[3]}" ]; then
      mkdir -p "${HOST[3]}"
    fi

    sed -i "s#{{WEBROOT}}#${HOST[3]}#g" "$DEST"
    sed -i "s#{{HOST}}#${HOST[6]:-http}://${HOST[4]}:${HOST[5]:-80}#g" "$DEST"
    sed -i "s#{{HOST_DOMAIN}}#${HOST_DOMAIN}#g" "$DEST"

    feature_auth
    feature_include
}

host_static() {
    sed -i "s#{{WEBROOT}}#${HOST[3]}#g" "$DEST"

    feature_auth
    feature_include
}

feature_include() {
    INCLUDE_VAR="${HOST[0]}_INCLUDE"
    INCLUDE="${!INCLUDE_VAR}"

    if [ -z "${INCLUDE}" ]; then
        sed -i "s#{{INCLUDE}}##g" "${DEST}"
        return
    fi

    INCLUDE="/etc/nginx/includes/${INCLUDE}.conf"

    sed -i "s#{{INCLUDE}}#include ${INCLUDE};#g" "${DEST}"
}

feature_auth() {
    AUTH_VAR="${HOST[0]}_AUTH"
    AUTH="${!AUTH_VAR}"

    if [ -z "${AUTH}" ]; then
        sed -i "s#{{AUTH}}##g" "${DEST}"
        return
    fi

    AUTH="/etc/nginx/auth/${AUTH}.htpasswd"

    sed -i "s#{{AUTH}}#auth_basic '${HOST[0]}'; auth_basic_user_file ${AUTH};#g" "${DEST}"
}

ssl_gen() {
    echo "No SSL cert found for $1, generating..."
    apk add --no-cache openssl \
        && mkdir "/etc/letsencrypt/live/$1" -p \
        && openssl req \
           -batch -newkey rsa:2048 -nodes -x509 -days 365 \
           -out "/etc/letsencrypt/live/$1/fullchain.pem" \
           -keyout "/etc/letsencrypt/live/$1/privkey.pem" \
           -subj "/CN=$1" \
        && apk del --no-cache openssl
}

generate_resolvers() {
    echo resolver "$(awk 'BEGIN{ORS=" "} $1=="nameserver" {print $2}' /etc/resolv.conf)" ";" > /etc/nginx/resolvers.conf
}
